﻿using System;
using System.IO;
using System.Text;
using TiledToOrx.Core;
using Microsoft.Extensions.CommandLineUtils;

namespace TiledToOrx.CLI
{
    class Program
    {
        static void Main(string[] args)
        {
            var app = new CommandLineApplication();
            app.Name = "TiledToOrx";
            app.Description = "Convert TMX files to Orx config data";
            app.HelpOption("-?|-h|--help");

            var fileName = app.Option(
                "-f|--fileName",
                "Path to the tmx file",
                CommandOptionType.SingleValue);

            var mapOnly = app.Option(
                "-m|--mapOnly",
                "Just output map data",
                CommandOptionType.NoValue);

            var oneBasedIndex = app.Option(
                "-o|--oneBasedIndex",
                "Start tile index at 1",
                CommandOptionType.NoValue);

            app.OnExecute(() => {
                StringBuilder sb = new StringBuilder();
                Engine engine = new Engine();

                if (fileName.HasValue())
                {
                    try
                    {
                        engine.Convert(fileName.Value(), sb, mapOnly.HasValue(), oneBasedIndex.HasValue());

                        if (engine.Errors.Length > 0)
                        {
                            Console.WriteLine($"Failed to convert file: {engine.Errors.ToString()}");
                            return 1;
                        }

                        string outputFile = fileName.Value().Replace(".tmx", ".ini");

                        using (var writer = File.CreateText(outputFile))
                        {
                            writer.Write(sb.ToString());
                        }

                        Console.WriteLine($"Output written to {outputFile}");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
                else
                {
                    app.ShowHint();
                }

                return 0;
            });

            app.Execute(args);


        }
    }
}
