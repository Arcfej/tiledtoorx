﻿using System;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Text;
using System.Windows.Forms;
using TiledToOrx.Core;

namespace TiledToOrx
{
    public partial class ConverterForm : Form
    {
        const string TAB = "\t";
        const string CR = "\n";
        const string AT = "@";

        readonly string version = "";
        readonly Engine engine = new Engine();


        public ConverterForm()
        {
            InitializeComponent();

            version = ConfigurationManager.AppSettings.Get("version");

            this.Text += version;
            engine.Watcher.Changed += Watcher_Changed;
            engine.MapEntityWidthPropertyChanged += Engine_MapEntityWidthPropertyChanged;
        }

        private void Engine_MapEntityWidthPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            mapEntitiesPerRowBox.Value = ((Engine)sender).EntitiesPerRow;
        }

        private void Watcher_Changed(object sender, FileSystemEventArgs e)
        {
            if (e.ChangeType == WatcherChangeTypes.Changed)
            {
                Invoke((MethodInvoker)delegate {
                    lastLoadedTextbox.Text = "Last loaded " + DateTime.Now.ToLocalTime();
                });
                CheckAndEnableAndUpdatePreview();
            }
        }

        private string ConvertTMXtoOrx()
        {
            if (tmxTextBox.Text.Length == 0)
                return "";

            StringBuilder sb = new StringBuilder();

            try
            {
                engine.Convert(tmxTextBox.Text, sb, mapOnlyCheckBox.Checked, oneBasedCheckBox.Checked);

                if (engine.Errors.Length > 0)
                {
                    MessageBox.Show(engine.Errors.ToString());
                    return "";
                }
            }
            catch (FileNotFoundException ex)
            {
                engine.Watcher.EnableRaisingEvents = false;
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                engine.Watcher.EnableRaisingEvents = false;
                MessageBox.Show("An Error occured loading and converting the TMX. Specific error: " + ex.Message);
            }

            return sb.ToString();
        }

        private void tiledBrowseButton_Click(object sender, EventArgs e)
        {

            OpenFileDialog dialog = new OpenFileDialog();
            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                string tmxFilePath = dialog.FileName;

                tmxTextBox.Text = tmxFilePath;
                tmxTextBox.Enabled = true;

                clipboardButton.Enabled = true;

            }
        }

        private void clipboardButton_Click(object sender, EventArgs e)
        {
            Clipboard.Clear();

            string output = "";

            if (previewBox.SelectionLength == 0)
            {
                output = previewBox.Text;
            }
            else
            {
                output = previewBox.SelectedText;
            }

            Clipboard.SetText(output);
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Tiled TMX to Orx Config v" + version + "\n2020 Wayne Johnson sausage@waynejohnson.net and contributors\nhttps://gitlab.com/sausagejohnson/tiledtoorx");
        }

        private void helpToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            string message = @"

This converter accepts a Tiled file and will generate Orx Config Data. All map formats are accepted: CSV, XML, Base64, Base64/gzip and Base64/zlib.

Use the loader button to choose a TMX file.

Conversion will appear in the window. You change the formatting with the checkbox options.

Check the 'Map Only' option if you want to generate only the map data. Good for repeat conversions without needing the graphic config info.

Check the '1 Based Index' checkbox if you want your map sections to begin with 0 or 1.

You can choose how many map index entities are placed in a row before starting a new row. By default, this is the amount of tiles across the map. But this can be any number. The width of a map and the indexes per row do not have to correlate. 

The TMX file that you select will be watched. If you continue to work on your map within Tiled Map Editor, it will update in the converter. Therefore you can keep the converter open to receive the latest changes.

Send bugs to: sausage@waynejohnson.net
or https://gitlab.com/sausagejohnson/tiledtoorx
or the Orx Discord Channel.
            ";

            MessageBox.Show(message);
        }

        private void CheckAndEnableAndUpdatePreview()
        {
            if (tmxTextBox.Text.Length == 0)
            {
                checkboxPanel.Enabled = false;
                clipboardButton.Enabled = false;
            }
            else
            {
                checkboxPanel.Enabled = true;
                clipboardButton.Enabled = true;
            }

            UpdatePreview();

        }

        private void UpdatePreview()
        {
            string output = ConvertTMXtoOrx();
            if (output.Length == 0)
            {
                this.Invoke((MethodInvoker)delegate {
                    previewBox.Text = "Did not convert.";
                });
            }
            else
            {
                this.Invoke((MethodInvoker)delegate {
                    previewBox.Text = output;
                });
            }
        }

        private void tmxTextBox_TextChanged(object sender, EventArgs e)
        {
            CheckAndEnableAndUpdatePreview();
        }

        private void oneBasedCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            CheckAndEnableAndUpdatePreview();
        }

        private void mapOnlyCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            CheckAndEnableAndUpdatePreview();
        }

        private void previewBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.A)
            {
                previewBox.SelectAll();
            }
        }

        private void mapEntitiesPerRow_ValueChanged(object sender, EventArgs e)
        {
            engine.EntitiesPerRow = (int)mapEntitiesPerRowBox.Value;
            CheckAndEnableAndUpdatePreview();
        }
    }
}
