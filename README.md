# Tiled to Orx
> Convert tmx files to Orx configuration

## CLI
### Build
#### Linux
	$ dotnet publish -c Release -r linux-64 --self-contained true
#### Windows
	$ dotnet publish -c Release -r win10-x64 --self-contained true
### Run
	$ ./TiledToOrx.CLI -f <filename>
### Options
- -f|--filename - Path to the tmx file
- -m|--mapOnly - Just output map data
- -o|--oneBasedIndex - Start tile index at 1